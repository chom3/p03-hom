//
//  ViewController.m
//  Breakout Clone
//
//  Created by Corey Hom on 2/11/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameScreen;
@synthesize score;
@synthesize lives;
@synthesize level;
@synthesize loseImage;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [gameScreen layoutIfNeeded];
    [gameScreen createPlayField:score:lives:level:loseImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
