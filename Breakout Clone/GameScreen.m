
//  GameScreen.m
//  Breakout Clone
//
//  Created by Corey Hom on 2/11/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import "GameScreen.h"

@implementation GameScreen
@synthesize paddle, ball;
@synthesize timer;
@synthesize rectFrame;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


UIView  *bricks[56];
int scoreInt = 0;
int levelInt = 1;
int livesInt = 3;
UILabel *passScore;
UILabel *passLives;
UILabel *passLevel;
UIImageView *loseScreen;
-(void)createPlayField: (UILabel*) score : (UILabel *) lives :(UILabel *) level :(UIImageView *) loseImage;
{
    [lives setText: [NSString stringWithFormat:@"%d", 3]];
    [score setText:[NSString stringWithFormat:@"%d", 0]];
    [level setText: [NSString stringWithFormat:@"%d", 1]];
    passLevel = level;
    passLives = lives;
    passScore = score;
    loseScreen = loseImage;
    loseScreen.hidden = YES;
    CGRect bounds = [self bounds];
    paddle = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-40, bounds.size.height-30, 60, 10)];
    [self addSubview:paddle];
    [paddle setBackgroundColor:[UIColor blackColor]];
    ball = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-15, bounds.size.height-40, 10, 10)];
    [self addSubview:ball];
    [ball setBackgroundColor:[UIColor redColor]];
    
    int brickX = 8;
    int brickY = 50;
    int k = 0;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(brickX, brickY, 50, 10)];
            bricks[k] = brick;
            k++;
            if (i == 0)
            {
                [brick setBackgroundColor: [UIColor redColor]];
            }
            else if ( i == 1)
            {
                [brick setBackgroundColor: [UIColor orangeColor]];
            }
            else if ( i == 2)
            {
                [brick setBackgroundColor: [UIColor yellowColor]];
            }
            else if ( i == 3)
            {
                [brick setBackgroundColor: [UIColor greenColor]];
            }
            else if ( i == 4)
            {
                [brick setBackgroundColor: [UIColor blueColor]];
            }
            else if ( i == 5)
            {
                [brick setBackgroundColor: [UIColor magentaColor]];
            }
            else if ( i == 6)
            {
                [brick setBackgroundColor: [UIColor purpleColor]];
            }
            else if (i == 7)
            {
                [brick setBackgroundColor: [UIColor cyanColor]];
            }
            [self addSubview:brick];
            brickX += 51;
        }
        brickY += 15;
        brickX = 8;
    }
    dx = 10;
    dy = 10;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGRect bounds = [self bounds];
    for (UITouch *t in touches){
        CGPoint p = [t locationInView: self];
        p.y = bounds.size.height-25;
        [paddle setCenter: p];
    }
}
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];
}


-(IBAction)startAnimation:(id)sender
{
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:.1	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

-(IBAction)stopAnimation:(id)sender
{
    [timer invalidate];
}

-(IBAction)resetAnimation:(id)sender
{
    [timer invalidate];
    [passScore setText:[NSString stringWithFormat:@"%d", 0]];
    [passLives setText: [NSString stringWithFormat:@"%d", 3]];
    loseScreen.hidden = YES;
    scoreInt = 0;
    livesInt = 3;
    CGRect bounds = [self bounds];
    paddle.hidden = YES;
    ball.hidden = YES;
    paddle = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-40, bounds.size.height-30, 60, 10)];
    [self addSubview:paddle];
    [paddle setBackgroundColor:[UIColor blackColor]];
    ball = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-15, bounds.size.height-40, 10, 10)];
    [self addSubview:ball];
    [ball setBackgroundColor:[UIColor redColor]];
    for (int i = 0; i < sizeof(bricks)/sizeof(bricks[0]); i++)
    {
        bricks[i].hidden = YES;
    }
    int brickX = 8;
    int brickY = 50;
    int k = 0;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(brickX, brickY, 50, 10)];
            bricks[k] = brick;
            k++;
            if (i == 0)
            {
                [brick setBackgroundColor: [UIColor redColor]];
            }
            else if ( i == 1)
            {
                [brick setBackgroundColor: [UIColor orangeColor]];
            }
            else if ( i == 2)
            {
                [brick setBackgroundColor: [UIColor yellowColor]];
            }
            else if ( i == 3)
            {
                [brick setBackgroundColor: [UIColor greenColor]];
            }
            else if ( i == 4)
            {
                [brick setBackgroundColor: [UIColor blueColor]];
            }
            else if ( i == 5)
            {
                [brick setBackgroundColor: [UIColor magentaColor]];
            }
            else if ( i == 6)
            {
                [brick setBackgroundColor: [UIColor purpleColor]];
            }
            else if (i == 7)
            {
                [brick setBackgroundColor: [UIColor cyanColor]];
            }
            [self addSubview:brick];
            brickX += 51;
        }
        brickY += 15;
        brickX = 8;
    }
    dx = 10;
    dy = 10;

}
-(void)timerEvent:(id)sender
{
    CGRect bounds = [self bounds];
    
    // NSLog(@"Timer event.");
    CGPoint p = [ball center];
    
    if ((p.x + dx) < 0)
        dx = -dx;
    
    if ((p.y + dy) < 0)
        dy = -dy;
    
    if ((p.x + dx) > bounds.size.width)
        dx = -dx;
    
    if ((p.y + dy) > bounds.size.height){
        dx = 0;
        dy = 0;
        livesInt--;
        [passLives setText:[NSString stringWithFormat:@"%d",livesInt]];
    }
    if (dx == 0 && dy == 0)
    {
        [timer invalidate];
        CGRect bounds = [self bounds];
        paddle.hidden = YES;
        ball.hidden = YES;
        paddle = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-40, bounds.size.height-30, 60, 10)];
        [self addSubview:paddle];
        [paddle setBackgroundColor:[UIColor blackColor]];
        ball = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-25, bounds.size.height-30.5, 10, 10)];
        [self addSubview:ball];
        [ball setBackgroundColor:[UIColor redColor]];
        p = [ball center];
        dx = 10;
        dy = 10;
    }
    if (livesInt == 0)
    {
        [timer invalidate];
        for (int i = 0; i < sizeof(bricks)/sizeof(bricks[0]); i++)
        {
            bricks[i].hidden = YES;
        }
        paddle.hidden = YES;
        ball.hidden = YES;
        [passScore setText:[NSString stringWithFormat:@"%s", "YOU LOSE!"]];
        loseScreen.hidden = NO;
    }
    p.x += dx;
    p.y += dy;
    [ball setCenter:p];
    
    // Now check to see if we intersect with paddle.  If the movement
    // has placed the ball inside the paddle, we reverse that motion
    // in the Y direction.
    if (CGRectIntersectsRect([ball frame], [paddle frame]))
    {
        dy = -dy;
        p.y += 2*dy;
        [ball setCenter:p];
    }
    //Check if intersects with a brick
    for (int i = 0; i < (sizeof bricks/sizeof bricks[0]); i++)
    {
        if (bricks[i].hidden == NO && CGRectIntersectsRect([ball frame], [bricks[i] frame]))
        {
            bricks[i].hidden = YES;
            dy = -dy;
            p.y += 2*dy;
            [ball setCenter:p];
            if (bricks[i].backgroundColor == [UIColor redColor])
            {
                scoreInt += 80;
            }
            if (bricks[i].backgroundColor == [UIColor orangeColor])
            {
                scoreInt += 70;
            }
            if (bricks[i].backgroundColor == [UIColor yellowColor])
            {
                scoreInt += 60;
            }
            if (bricks[i].backgroundColor == [UIColor greenColor])
            {
                scoreInt += 50;
            }
            if (bricks[i].backgroundColor == [UIColor blueColor])
            {
                scoreInt += 40;
            }
            if (bricks[i].backgroundColor == [UIColor magentaColor])
            {
                scoreInt += 30;
            }
            if (bricks[i].backgroundColor == [UIColor purpleColor])
            {
                scoreInt += 20;
            }
            if (bricks[i].backgroundColor == [UIColor cyanColor])
            {
                scoreInt += 10;
            }
            [passScore setText:[NSString stringWithFormat:@"%d", scoreInt]];
        }
    }
    //Check if Game is over!
    int winCount = 0;
    for (int i = 0; i < (sizeof bricks/sizeof bricks[0]); i++)
    {
        if (bricks[i].hidden == YES)
        {
            winCount++;
        }
    }
    if (winCount == (sizeof bricks/sizeof bricks[0]))
    {
        if (loseScreen.hidden == YES){
            [timer invalidate];
            levelInt++;
            livesInt++;
            CGRect bounds = [self bounds];
            paddle.hidden = YES;
            ball.hidden = YES;
            paddle = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-40, bounds.size.height-30, 60, 10)];
            [self addSubview:paddle];
            [paddle setBackgroundColor:[UIColor blackColor]];
            ball = [[UIView alloc] initWithFrame:CGRectMake((bounds.size.width/2)-15, bounds.size.height-40, 10, 10)];
            [self addSubview:ball];
            [ball setBackgroundColor:[UIColor redColor]];
            p = [ball center];
            dx = 10;
            dy = 10;
            [passLevel setText:[NSString stringWithFormat:@"%d", levelInt]];
            [passLives setText:[NSString stringWithFormat:@"%d", livesInt]];
            for (int i = 0; i < sizeof(bricks)/sizeof(bricks[0]); i++)
            {
                bricks[i].hidden = YES;
            }
            int brickX = 8;
            int brickY = 50;
            int k = 0;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(brickX, brickY, 50, 10)];
                    bricks[k] = brick;
                    k++;
                    if (i == 0)
                    {
                        [brick setBackgroundColor: [UIColor redColor]];
                    }
                    else if ( i == 1)
                    {
                        [brick setBackgroundColor: [UIColor orangeColor]];
                    }
                    else if ( i == 2)
                    {
                        [brick setBackgroundColor: [UIColor yellowColor]];
                    }
                    else if ( i == 3)
                    {
                        [brick setBackgroundColor: [UIColor greenColor]];
                    }
                    else if ( i == 4)
                    {
                        [brick setBackgroundColor: [UIColor blueColor]];
                    }
                    else if ( i == 5)
                    {
                        [brick setBackgroundColor: [UIColor magentaColor]];
                    }
                    else if ( i == 6)
                    {
                        [brick setBackgroundColor: [UIColor purpleColor]];
                    }
                    else if (i == 7)
                    {
                        [brick setBackgroundColor: [UIColor cyanColor]];
                    }
                    [self addSubview:brick];
                    brickX += 51;
                }
                brickY += 15;
                brickX = 8;
            }
            dx = 10;
            dy = 10;
        }
    }
    
}

@end
