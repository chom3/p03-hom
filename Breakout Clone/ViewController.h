//
//  ViewController.h
//  Breakout Clone
//
//  Created by Corey Hom on 2/11/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *score;
@property (weak, nonatomic) IBOutlet UILabel *lives;
@property (weak, nonatomic) IBOutlet UILabel *level;
@property (weak, nonatomic) IBOutlet UIImageView *loseImage;
@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;
@end

