//
//  GameScreen.h
//  Breakout Clone
//
//  Created by Corey Hom on 2/11/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
    float dx, dy;  // Ball motion
}
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) UIView *rectFrame;

-(void)createPlayField:(UILabel*) score :(UILabel*) lives :(UILabel*) currentLevel :(UIImageView*) loseImage;

@end
