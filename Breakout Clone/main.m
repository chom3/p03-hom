//
//  main.m
//  Breakout Clone
//
//  Created by Corey Hom on 2/11/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
